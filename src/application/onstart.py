def on_start():
    from core.indexer import IndexerTask

    indexer_task = IndexerTask()
    indexer_task.run()