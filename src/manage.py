#!/usr/bin/env python
import os
import sys
import logging


if __name__ == "__main__":
    logger = logging.getLogger("root")
    LOG_FILE = "log.txt"
    FORMAT = '%(levelname)s:%(thread)d:%(message)s'

    formatter = logging.Formatter(FORMAT)
    hdlr = logging.FileHandler(LOG_FILE)
    hdlr.setFormatter(formatter)
    logger.setLevel(logging.DEBUG)
    logger.addHandler(hdlr)

    ch = logging.StreamHandler(sys.stdout)
    ch.setLevel(logging.DEBUG)
    ch.setFormatter(formatter)
    logger.addHandler(ch)

    os.environ.setdefault("DJANGO_SETTINGS_MODULE", "application.settings")
    try:
        from django.core.management import execute_from_command_line
    except ImportError:
        # The above import may fail for some other reason. Ensure that the
        # issue is really that Django is missing to avoid masking other
        # exceptions on Python 2.
        try:
            import django
        except ImportError:
            raise ImportError(
                "Couldn't import Django. Are you sure it's installed and "
                "available on your PYTHONPATH environment variable? Did you "
                "forget to activate a virtual environment?"
            )
        raise

    execute_from_command_line(sys.argv)

