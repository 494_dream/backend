from django.shortcuts import render
from core.tools import download_file
from django.http import HttpResponse
import json
import logging
import uuid
import threading
from django.views.decorators.csrf import csrf_exempt
from application.local_settings import FILES_DIR
from pdfrw import PdfReader, PdfWriter
from core.search_client import SearchClient
from core.models import Tag, FileInstance
from core.image import get_image_size
from wand.image import Image, Color
import os
import subprocess
import math


@csrf_exempt
def start_file_upload(request):
    print(str(request.POST))
    url = request.POST.get("url")
    tags = json.loads(request.POST.get("tags"))
    description = request.POST.get("description")
    if (description is None):
        description = ""
    filename = str(uuid.uuid4())
    download_file(url, filename, tags, description)
    return HttpResponse(json.dumps({"file_id": filename}),
                        content_type="application/json")

def get_file_pdf(request):
    filename = request.GET.get("id")
    file_in = os.path.join(FILES_DIR, filename + ".pdf")
    page_number = request.GET.get("page")
    if (page_number):
        page_number = int(page_number)
        #file_out = os.path.join(FILES_DIR, filename + "subset.png")
        #with Image(filename=file_in, resolution=300) as img:
        #    img.save(filename=file_out)
        #    response = HttpResponse(content=open(file_out, "rb"))
        #    response['Content-Type'] = 'image/png'
        #    return response
        file_out = os.path.join(FILES_DIR, filename + "subset.pdf")
        pages = PdfReader(file_in).pages
        #box = pages[0]['/MediaBox']
        outdata = PdfWriter()
        outdata.addpage(pages[page_number - 1])
        outdata.write(file_out)
        #print(pages[page_number - 1].xobj_box)
        #file_out_png = os.path.join(FILES_DIR, filename + "subset.png")
        #subprocess.call(['convert', '-density', '700', '-resize', '%sx%s' % (box[-2], box[-1]), file_out, file_out_png])
        #response = HttpResponse(content=open(file_out_png, "rb"))
        #response['Content-Type'] = 'image/png'
        #return response
        file_in = file_out

    response = HttpResponse(content=open(file_in, "rb"))
    response['Content-Type'] = 'application/pdf'
    response['Content-Encoding'] = 'utf-8'
    response['X-Frame-Options'] = 'GOFORIT'
    return response


def get_file_size(request):
    filename = request.GET.get("id")
    page_number = int(request.GET.get("page"))
    file_in = os.path.join(FILES_DIR, filename + ".pdf")
    file_out = os.path.join(FILES_DIR, filename + "subset.pdf")
    pages = PdfReader(file_in).pages
    outdata = PdfWriter()
    outdata.addpage(pages[page_number - 1])
    outdata.write(file_out)
    file_out_png = os.path.join(FILES_DIR, filename + "subset.png")
    subprocess.call(['convert', '-density', '96', '-quality', '85', '-flatten', file_out, file_out_png])
    return HttpResponse(json.dumps("[%d, %d]" % get_image_size(file_out_png)), content_type="application/json")


def get_file_image(request):
    filename = request.GET.get("id")
    file_in = os.path.join(FILES_DIR, filename + ".pdf")
    page_number = request.GET.get("page")
    if (page_number):
        page_number = int(page_number)
        #file_out = os.path.join(FILES_DIR, filename + "subset.png")
        #with Image(filename=file_in, resolution=300) as img:
        #    img.save(filename=file_out)
        #    response = HttpResponse(content=open(file_out, "rb"))
        #    response['Content-Type'] = 'image/png'
        #    return response
        file_out = os.path.join(FILES_DIR, filename + "subset.pdf")
        pages = PdfReader(file_in).pages
        outdata = PdfWriter()
        outdata.addpage(pages[page_number - 1])
        outdata.write(file_out)
        file_out_png = os.path.join(FILES_DIR, filename + "subset.png")
        subprocess.call(['convert', '-density', '96', '-quality', '85', '-flatten', file_out, file_out_png])
        response = HttpResponse(content=open(file_out_png, "rb"))
        response['Content-Type'] = 'image/png'
        return response
        file_in = file_out

    response = HttpResponse(content=open(file_in, "rb"))
    response['Content-Type'] = 'application/pdf'
    response['X-Frame-Options'] = 'GOFORIT'
    return response


def get_tutors(request):
    tutors = Tag.objects.filter(
        tag_name__startswith="Tutor:").order_by('tag_name')
    return HttpResponse(json.dumps([x.tag_name[len("Tutor: "):]
                                    for x in tutors]),
                        content_type="application/json")


def get_courses(request):
    courses = Tag.objects.filter(tag_name__startswith="Course:").all()
    return HttpResponse(json.dumps([x.tag_name[len("Course: "):]
                                    for x in courses]),
                        content_type="application/json")


def sign(x):
   if (x < 0):
      return -1
   if (x > 0):
      return 1
   return 0


def calculate_relevance(rating, score):
    return [sign(rating), 3 * score + sign(rating) * math.log(1 + abs(rating))]


def search(request):
    tags = request.GET.get("tags")
    if (tags is None or len(tags) == 0):
        tags = []
    else:
        tags = tags.split(",")
    query = request.GET.get("q")
    if (query is None):
        query = ""
    data = {"text": query, "tags": tags}
    results = SearchClient().find(data)
    logging.getLogger("root").debug(str(results))
    final_results = []
    for result in results:
        dbresult = FileInstance.objects.get(filename=result["id"])
        if (query == ""):
            result["page"] = [1]
        result["description"] = dbresult.description
        result["tags"] = [x.tag_name for x in dbresult.tags.all()]
        result["rating"] = dbresult.rating
        result["heavy"] = dbresult.heavy
        result["criteria"] = calculate_relevance(result["rating"], result["score"])
    results.sort(key=lambda x: calculate_relevance(x["rating"], x["score"]), reverse=True)
    return HttpResponse(json.dumps(results),
                        content_type="application/json")


def get_elastic(request):
    id = request.GET.get("id")
    return HttpResponse(str(SearchClient().get(id)))


@csrf_exempt
def change_rating(request):
    delta = request.POST.get("delta")
    filename = request.POST.get("id")
    try:
        delta = int(delta)
        if (abs(delta) != 1):
            return HttpResponse("Bad request")
        instance = FileInstance.objects.get(filename=filename)
        instance.rating += delta
        instance.save()
        return HttpResponse(json.dumps({"rating": instance.rating}),
                            content_type="application/json")
    except Exception:
        return HttpResponse("Bad request")


def get_rating(request):
    filename = request.GET.get("id")
    try:
        instance = FileInstance.objects.get(filename=filename)
        return HttpResponse(json.dumps({"rating": instance.rating}),
                            content_type="application/json")
    except Exception:
        return HttpResponse("Bad request")
