from django.db import models

# Create your models here.


class Tag(models.Model):
    tag_name = models.CharField(max_length=50)

    def __str__(self):
        return self.tag_name


class FileInstance(models.Model):
    filename = models.CharField(max_length=50)
    description = models.CharField(default="", max_length=140)
    tags = models.ManyToManyField(Tag)
    rating = models.IntegerField(default=0)
    indexed = models.BooleanField(default=False)
    heavy = models.BooleanField(default=False)
    def __str__(self):
        return self.filename
