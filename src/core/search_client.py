from elasticsearch import Elasticsearch
import logging
import json


class SearchClient:
    def __init__(self):
        self.logger = logging.getLogger("root")
        self.es = Elasticsearch()

    def find(self, query):
        self.logger.debug(str(query))
        query_params = [{
                'match': {
                    'attachment.content': {
                        'query': query["text"],
                        'fuzziness': (1 if len(query["text"]) < 7 else 2),
                        'operator': 'and',
                        'zero_terms_query': 'all'
                    }
                }
        }]
        for i in query["tags"]:
            query_params.append({
                'match_phrase': {
                    'tags': i
                }
            })
        es_query = {
            'size': 0,
            '_source': ['id', 'page'],
            'query': {
                "bool": {
                    'must': query_params
                }
            },
            "aggs": {
                'group_by_id': {
                    "terms": {
                        'field': 'id',
                        'order': {
                            'top_score': 'desc'
                        }
                    },
                    'aggs': {
                        'top_score': {
                            'max': {
                                'script': {
                                    'inline': '_score'
                                }
                            }
                        },
                        'docs': {
                            'top_hits': {
                                '_source': ['page', 'id'],
                                'size': 10
                            }
                        }
                    }
                }
            }
        }
        response = self.es.search(index='files', body=es_query)
        self.logger.debug(str(response));
        buckets = response['aggregations']['group_by_id']['buckets']
        results = []
        for bucket in buckets:
            hits = bucket['docs']['hits']['hits']
            pages = [hit['_source']['page'] for hit in hits]
            result = {
                'id': hits[0]['_source']['id'],
                'score': bucket['docs']['hits']['max_score'],
                'page': pages
            }
            results.append(result)
        return results

    def get(self, id):
        result = self.es.get(index='files', doc_type='pdf', id=id)
        return result
