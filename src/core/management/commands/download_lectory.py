from django.core.management.base import BaseCommand, CommandError
from core.crawlers.lectoriy import Crawler

class Command(BaseCommand):

    def handle(self, *args, **options):
        Crawler().download_docs()
