from django.conf.urls import url, include

from core.views import start_file_upload, get_file_pdf, get_file_image, search, get_tutors, \
                       get_courses, get_elastic, change_rating, get_rating, get_file_size

urlpatterns = [
    url(r'upload/', start_file_upload),
    url(r'get/', get_file_pdf),
    url(r'get_img/', get_file_image),
    url(r'get_elastic/', get_elastic),
    url(r'get_tutors/', get_tutors),
    url(r'get_courses/', get_courses),
    url(r'search/', search),
    url(r'change_rating/', change_rating),
    url(r'get_rating/', get_rating),
    url(r'get_size/', get_file_size)
]
