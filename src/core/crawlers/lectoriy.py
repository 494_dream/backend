import requests
import json
from core.models import FileInstance
import uuid
from core.tools import download_file
import logging


class Crawler:
    def __init__(self):
        self.logger = logging.getLogger("root")
        token = ''
        with open('lectoriy.token', 'r') as token_file:
            token = token_file.read().strip()
        self.token = token
    def get_page(self, page):
        url = 'http://lectoriy.mipt.ru/api/v1/material?token=' + self.token + '&expand=properties,lectures,lecturers,collections,courses&per-page=100&page=' + str(page)
        r = requests.get(url)
        return r.text

    def download_docs(self):
        current_page = 1
        while True:
            page = self.get_page(current_page)
            docs = json.loads(page)
            for doc in docs:
                id = str(uuid.uuid4())
                filepath = doc['file']
                tmp = filepath[:filepath.rfind('-')]
                author = tmp[tmp.rfind('-') + 1:]
                course = doc['category']['title']
                description = doc['lectures'][0]['title']
                tags = [
                    'Course: {}'.format(course),
                    'Tutor: {}'.format(author)
                ]
                #self.logger.debug('{} {} {} {}'.format(id, filepath, description, tags))
                download_file(filepath, id, tags, description)
            if len(docs) < 100:
                break
            current_page += 1