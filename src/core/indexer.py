from core.tools import async_run
from time import sleep
import logging
from application import local_settings
import os
import base64
from core.models import FileInstance
from elasticsearch import Elasticsearch
from pdfrw import PdfReader, PdfWriter

class IndexerTask:
    def __init__(self):
        self.es = Elasticsearch()
        self.es.indices.create(index='files', ignore=[400], body={
            'mappings': {
                'pdf': {
                    'properties': {
                        'id': {
                            'type': 'string',
                            'index': 'not_analyzed'
                        }
                    }
                }
            }
        })
        self.es.ingest.put_pipeline(id='index_file', body={
            'description': 'Extract file information',
            'processors': [
                {
                    'attachment': {
                        'field': 'data',
                        'indexed_chars': -1
                    }
                }
            ]
        })
        self.logger = logging.getLogger("root")
        self.logger.debug('Done loading ES')

    def get_elastic_id(self, file_id, page_id):
        return str(file_id) + '_' + str(page_id)

    def get_new_task(self):
        task = FileInstance.objects.filter(indexed=False).first()
        return task

    def get_task_tags(self, task):
        return [x.tag_name for x in task.tags.all()]

    def get_file_data(self, file_id):
        filename = file_id + '.pdf'
        filepath = os.path.join(local_settings.FILES_DIR, filename)
        tempfile = os.path.join(local_settings.FILES_DIR, 'temp.pdf')
        reader = PdfReader(filepath)
        pages_content = []
        for page in reader.pages:
            writer = PdfWriter()
            writer.addpage(page)
            writer.write(tempfile)
            with open(tempfile, 'rb') as doc_file:
                file_content = base64.encodebytes(doc_file.read())\
                    .decode('ASCII').replace('\n', '')
                pages_content.append(file_content)
        return pages_content

    def index(self, document_id, data):
        self.logger.debug("Indexing file")
        self.es.index(
            index='files',
            id=document_id,
            doc_type='pdf',
            body=data,
            pipeline='index_file',
            timeout='1s'
        )

    def process_page(self, file_id, page_id, page_data, tags):
        data = {
            'id': file_id,
            'page': page_id,
            'tags': ';'.join(tags),
            'data': page_data,
        }
        self.index(self.get_elastic_id(file_id, page_id), data)

    def process_task(self, task):
        file_id = task.filename
        tags = self.get_task_tags(task)
        pages_data = self.get_file_data(file_id)
        for page_id, page_data in enumerate(pages_data, 1):
            self.process_page(file_id, page_id, page_data, tags)
        task.indexed = True
        task.save()
        self.logger.debug('Done indexing file')

    @async_run
    def run(self):
        self.logger.debug("Started file indexing task")
        while True:
            task = self.get_new_task()
            if task:
                self.logger.debug("Processing new file")
                self.process_task(task)
            sleep(5)
