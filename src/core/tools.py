import urllib
import uuid
import os
from application import local_settings
import threading
import logging
from core.models import FileInstance, Tag
from threading import Lock
from pdfrw import PdfReader, PdfWriter

lock = Lock()

def async_run(function):
    def wrapper(*args, **kwargs):
        t = threading.Thread(target=function,
                             args=args,
                             kwargs=kwargs)
        t.setDaemon(True)
        t.start()
    return wrapper


def get_vk_url(url, logger):
    page = urllib.request.urlopen(url).read().decode('cp1251').split('\n')
    for i in page:
        pos = i.find('var src = ')
        if pos != -1:
            url = i[pos + len('var src = ') + 1:-1]
            logger.debug("VK URL: " + url)
            return url


@async_run
def download_file(url, id, tags, description):
    logger = logging.getLogger("root")
    logger.debug("IN" + str(url) + str(id))
    logger.debug(str(tags) + " " + str(description))
    try:
        if urllib.parse.urlparse(url).netloc == "vk.com":
            url = get_vk_url(url, logger)
        urllib.request.urlretrieve(url,
                                   os.path.join(local_settings.FILES_DIR, id +
                                                ".pdf"))
        file_in = os.path.join(local_settings.FILES_DIR, id + ".pdf")
        size = os.stat(os.path.join(local_settings.FILES_DIR, id + ".pdf")).st_size
        pages = len(PdfReader(file_in).pages)
        heavy = False
        if (size / pages > 1024 * 100):
            heavy = True
        lock.acquire()
        file_instance = FileInstance(filename=id, description=description, heavy=heavy)
        file_instance.save()
        for tag in tags:
            if (not len(Tag.objects.filter(tag_name=tag))):
                Tag(tag_name=tag).save()
            file_instance.tags.add(Tag.objects.get(tag_name=tag))
            logger.debug("added %s" % tag)
        lock.release()
        logger.debug("Done")
    except Exception as e:
        logger.exception(str(e))
