from django.contrib import admin
from core.models import Tag, FileInstance
# Register your models here.

admin.site.register(Tag)
admin.site.register(FileInstance)
